package com.ebsw.example_kafka_producer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * Kafka Producer example
 * This writes a simple string to the  "test" topic. example kafka consumer reads a simple string from that topic
 *
 *To run this just run as Maven Build with clean package
 * then go to target directory and execute   java -jar example_kafka_producer-0.0.1-SNAPSHOT.jar
 * the pom.xml defines this class as the Main
 */
public class App 
{
	public static String getCurrentTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		return (dtf.format(now));  
	}
	
    public static void main( String[] args )
    { 
    	try {
            System.out.println( "Hello World!" );
            Properties props = new Properties();
//            props.put("bootstrap.servers", "192.168.99.100:9092");
            props.put("bootstrap.servers", "192.168.1.14:32770");
            props.put("acks", "all");
            props.put("retries", 0);
            props.put("batch.size", 16384);
            props.put("linger.ms", 1);
            props.put("buffer.memory", 33554432); 
            props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

            Producer<String, String> producer = new KafkaProducer<>(props);
            String valuemsg = "";
            
           	valuemsg = "{\"search_request_id\" : \"" + System.currentTimeMillis() + "\"," +
           				"\"claim_number\" : \"Cen-123456789001\", " +
           				"\"lob\" : \"Centene\"," +
           				"\"userid\" : \"nordj\"}";
           	System.out.println(valuemsg);
            producer.send(new ProducerRecord<String, String>("search-request", valuemsg, valuemsg));
 
            producer.close();    		
            System.out.println("The End is near"); 
    	} catch(Exception e) {
    		System.out.println(e.getMessage());
    	}

    }
}
